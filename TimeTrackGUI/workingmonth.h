#ifndef WORKINGMONTH_H
#define WORKINGMONTH_H

#include <QDebug>
#include <QList>
#include "workingday.h"

/**
 * @brief Отработанный месяц.
 * @detailed Состоит из WorkingDay, относящихся к отдельному месяцу.
 */

class WorkingMonth
{
public:
    /**
     * @brief WorkingMonth - отработанный месяц. Не может быть создан, если в
     * него не входит ни один месяц.
     * @param day - WorkingDay, который относится (по дате) к этому WorkingMonth.
     */
    explicit WorkingMonth(WorkingDay &day);

    ///Добавить отработанный день к текущему WorkingMonth.
    int addDay(WorkingDay &day);

    ///Численное обозначение текущего месяца (1-12).
    int getMonth() {return month;}

    ///Год текущего месяца.
    int getYear() {return year;}

    ///Подготовленная строка месяц + год.
    QString getMonthCaption();

    ///Подготовленная строка с отработанным временем за месяц:
    /// "часы, минуты (рабочие дни, часы)". Рабочий день = 8 часов.
    QString getWorkedTimeStr();

private:
    ///Список из отработанных дней, входящих в текущий месяц.
    QList<WorkingDay> working_days;

    ///Численное обозначение текущего месяца (1-12).
    int month;

    ///Год текущего месяца.
    int year;

    ///Отработанное время в миллисекундах.
    qint64 getWorkedTime(); //in seconds
};

#endif // WORKINGMONTH_H
