#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    statistic_window(NULL),
    time_counter(new TimeCounter()),
    timer(new QTimer)
{
    ui->setupUi(this);    
    ui->lcdNumber->setVisible(false);
    ui->checkBox_2->setVisible(false);

    ui->lcdNumber_2->display(time_counter->timeToGoAway().toString("hh:mm:ss"));

    connect(timer, SIGNAL(timeout()), this, SLOT(on_timer()));
}


MainWindow::~MainWindow()
{    
    delete ui;
    if (statistic_window != NULL) {delete statistic_window; statistic_window = NULL;}
    delete time_counter;
    delete timer;    
}


void MainWindow::on_timer()
{
    ui->lcdNumber->display(time_counter->workedToday());
}


void MainWindow::on_checkBox_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked)
    {       
        bool need_to_subtract_dinner = time_counter->analyzeIsItNeededToSubtractDinner();
        ui->checkBox_2->QAbstractButton::setChecked(need_to_subtract_dinner);
        ui->checkBox_2->setVisible(true);

        on_timer(); //первое значение обновим вручную (чтобы не ждать секунду до обновления)
        ui->lcdNumber->setVisible(true);

        timer->start(1000); //таймер срабатывает 1 раз в секунду
    }
    else if (arg1 == Qt::Unchecked)
    {
        ui->lcdNumber->setVisible(false);
        ui->checkBox_2->setVisible(false);
        timer->stop();
    }
}


void MainWindow::on_checkBox_2_clicked()
{
    bool result;

    if (ui->checkBox_2->isChecked())
    {
        result = time_counter->set_NeedToSubtractDinner(true);
    }
    else
    {
        result = time_counter->set_NeedToSubtractDinner(false);
    }

    ui->checkBox_2->QAbstractButton::setChecked(result);
}



void MainWindow::on_pushButton_clicked()
{
    QApplication::quit();
}


void MainWindow::on_pushButton_2_clicked()
{
    if (statistic_window == NULL) statistic_window = new StatisticWindow();
    statistic_window->show();
}
