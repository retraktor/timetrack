#ifndef STATISTICWINDOW_H
#define STATISTICWINDOW_H

#include <QCloseEvent>
#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QMainWindow>
#include <QMessageBox>
#include <QShowEvent>
#include <QTableWidgetItem>

#include "databasecontroller.h"
#include "emptydaydialog.h"


namespace Ui {
class StatisticWindow;
}

/**
 * @brief Окно статистики. Оно отображает отработанные дни и переработку.
 */

class StatisticWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit StatisticWindow(QWidget *parent = 0);
    ~StatisticWindow();

public slots:
    ///Внесет в таблицу столбец с датами.
    void fillTableWithDates();

    ///Внесет в таблицу столбцы с началом и окончанием отработанного дня.
    void fillTableWithSessions();

    ///Внесет в таблицу столбец с отработанным временем за день.
    void fillTableWithDurations();

    ///Заполнит таблицу отработанными месяцами.
    void fillTableWithMonths();

    ///Внесет в таблицу столбец с переработками.
    void fillTableWithOvers();


    ///Очистит таблицу.
    void cleanTable();

    ///Обновит таблицу (заново запросит данные у DataBaseController).
    void refillTable();


    ///Для добавления пустого дня.
    void addEmptyDay(const QDate &date);


protected:

    ///При первом показе формы. Заполнит таблицу.
    void showEvent(QShowEvent *ev);

    ///При закрытии формы. Очистит таблицу.
    void closeEvent(QCloseEvent *ev);


private slots:
    void on_cb_month_statistic_stateChanged(int arg1);

    void on_pb_write_to_database_clicked();

    void on_tableWidget_itemChanged(QTableWidgetItem *item);

    void on_pb_add_empty_day_clicked();
    void on_pb_delete_day_clicked();

    void on_cb_current_month_stateChanged(int arg1);

private:
    Ui::StatisticWindow *ui;


    const QString date_str;
    const QString started_str;
    const QString finished_str;
    const QString need_to_work_str;



    bool table_prepared;

    void addTableItem(int row, int col, QString str, bool read_only = true, bool highlighting = false);
};

#endif // STATISTICWINDOW_H
