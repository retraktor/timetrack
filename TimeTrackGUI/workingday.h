#ifndef WORKINGDAY_H
#define WORKINGDAY_H

#include <QDate>
#include <QTime>

#include "settings.h"

/**
 * @brief Отработанный день.
 * @detailed Каждый день должен иметь уникальную дату.
 * TimeTrackDaemon обеспечивает эту уникальность на уровне базы данных.
 * Дата начала отработанного дня должна совпадать с датой конца отработанного
 * дня.
 */

class WorkingDay
{
public:
    /**
     * @brief WorkingDay - отработанный рабочий день. Даты started и finished
     * должны совпадать.
     * @param id - id рабочего дня в соответствии с базой данных.
     * @param started - начало рабочего дня.
     * @param finished - конец рабочего дня.
     * @param need_to_work - сколько необходимо отработать за этот день (в
     * миллисекундах).
     */
    explicit WorkingDay(int id, QDateTime started, QDateTime finished, int need_to_work);

    ///Id отработанного дня в соответствии с базой данных.
    int      getID()            {return id_in_database;}

    ///Дата отработанного дня.
    QDate &  getDate()          {return date;          }

    ///Время начала отработанного дня.
    QTime &  getStartedTime()   {return started_time;  }

    ///Время конца рабочего дня.
    QTime &  getFinishedTime()  {return finished_time; }

    ///Сколько необходимо отработать за этот день (в миллисекундах).
    int      getNeedToWork()    {return need_to_work_; }

    ///Отработанное время (обед уже вычтен).
    QTime &  getWorkedTime()    {return worked_time;   }


    ///Начало отработанного дня в миллисекундах. Понадобится для сравнения
    /// WorkingDay между собой.
    qint64   getStarted() const {return started_;      }

    ///WorkingDay сравниваются по времени начала рабочего дня в миллисекундах.
    bool operator <(const WorkingDay &wd2) const{
        return (started_ < wd2.getStarted());
    }

private:
    ///Id отработанного дня в соответствии с базой данных (для обратного связи
    /// с базой данных: чтобы изменять конкретные отработанные дни).
    int    id_in_database;

    ///Дата отработанного дня.
    QDate  date;

    ///Время начала отработанного дня.
    QTime  started_time;

    ///Время конца рабочего дня.
    QTime  finished_time;

    ///Сколько необходимо отработать за этот день (в миллисекундах).
    int    need_to_work_; //in msecs

    ///Отработанное время без вычета обеда.
    QTime  duration;      //duration = started_time - finished_time

    ///Отработанное время (обед уже вычтен).
    QTime  worked_time;   //worked_time = durattion - dinner_duration

    ///Начало отработанного дня (в миллисекундах). Понадобится для сравнения
    /// WorkingDay между собой.
    qint64 started_;      //to compare WorkingDays
};

#endif // WORKINGDAY_H
