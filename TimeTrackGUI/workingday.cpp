#include "workingday.h"
#include <QDebug>

WorkingDay::WorkingDay(int id, QDateTime started, QDateTime finished, int need_to_work) :
    id_in_database(id),
    date(started.date()),
    started_time(started.time()),
    finished_time(finished.time()),
    need_to_work_(need_to_work),
    duration(QTime::fromMSecsSinceStartOfDay(finished.toMSecsSinceEpoch()-started.toMSecsSinceEpoch())),
    worked_time(),
    started_(started.toMSecsSinceEpoch())
{
    qint64 worked_secs = duration.hour() * 60*60 +
                         duration.minute() * 60 +
                         duration.second() -
                         Settings::Instance().get_dinner_duration().hour() * 60*60 -
                         Settings::Instance().get_dinner_duration().minute() * 60 -
                         Settings::Instance().get_dinner_duration().second();
    worked_time = QTime::fromMSecsSinceStartOfDay(worked_secs * 1000);
}

