#include "emptydaydialog.h"
#include "ui_emptydaydialog.h"

EmptyDayDialog::EmptyDayDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EmptyDayDialog)
{
    ui->setupUi(this);
    ui->dateEdit->setDate(QDateTime::currentDateTime().date());

}


EmptyDayDialog::~EmptyDayDialog()
{
    delete ui;
}


void EmptyDayDialog::on_buttonBox_accepted()
{
     emit selected_date(ui->dateEdit->date());
}
