#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QString>
#include <QtSql>


/**
 * @brief Класс Settings содержит в себе всё то, что можно вынести в настройки.
 * @detailed А именно:
 * <ul>
 * <li> путь к базе данных;
 * <li> продолжительность обеда;
 * <li> продолжительность рабочего дня;
 * <li> формат вывода в строку даты;
 * <li> формат вывода в строку времени.
 * </ul>
 *
 * Реализуется в виде Singleton. Для обращения к методам класса сначала
 * необходимо получить Instance: Settings::Instance().
 */

class Settings
{

public:

    ///Используется Singleton. Для использования методов необходимо получить
    /// Instance: Settings::Instance().
    static Settings& Instance()
    {
      static Settings singleton;
      return singleton;
    }

    ///Путь к базе данных (вместе с именем файла).
    QString & get_database_path()   {return database_path;  }

    ///Продолжительность обеда.
    QTime   & get_dinner_duration() {return dinner_duration;}

    ///Продолжительность обеда в миллисекундах.
    int get_dinner_duration_msecs() {return dinner_duration.msecsSinceStartOfDay();}

    ///Сколько необходимо отработать за 1 день (в миллисекундах).
    int get_need_to_work_msecs()    {return need_to_work;   }

    ///Формат времени.
    QString const & getDateFormat() {return date_format;}

    ////Формат даты.
    QString const & getTimeFormat() {return time_format;}

private:
    ///Запрет вызова конструктора (т.к. реализуем Singleton).
    Settings();

    ///Запрет вызова деструктора (т.к. реализуем Singleton).
    ~Settings() {}

    ///Запрет вызова конструктора копирования.
    Settings(const Settings&);

    ///Запрет присваивания.
    Settings& operator=(const Settings&);

    ///Для доступа к внешним настройкам (к файлу конфигурации).
    QSettings    qsettings;

    ///Путь к базе данных (вместе с именем файла).
    QString      database_path;

    ///Инициализируем базу данных здесь. Это потребуется в нескольких местах
    /// (в SQL запросах DataBaseController и TimeCounter).
    QSqlDatabase sdb;

    ///Продолжительность обеда.
    QTime        dinner_duration;

    ///Сколько необходимо отработать за 1 день (в миллисекундах).
    int          need_to_work;

    ///Формат времени.
    const QString time_format;

    ///Формат даты.
    const QString date_format;
};

#endif // SETTINGS_H

