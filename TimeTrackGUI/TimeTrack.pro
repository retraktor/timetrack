#-------------------------------------------------
#
# Project created by QtCreator 2014-03-09T15:29:17
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TimeTrack
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    timecounter.cpp \
    statisticwindow.cpp \
    workingday.cpp \
    settings.cpp \
    workingmonth.cpp \
    emptydaydialog.cpp \
    databasecontroller.cpp

HEADERS  += mainwindow.h \
    timecounter.h \
    statisticwindow.h \
    workingday.h \
    settings.h \
    workingmonth.h \
    emptydaydialog.h \
    databasecontroller.h

FORMS    += mainwindow.ui \
    statisticwindow.ui \
    emptydaydialog.ui

RC_FILE += res/stopwatch.rc

RESOURCES += \
    res/pics.qrc
