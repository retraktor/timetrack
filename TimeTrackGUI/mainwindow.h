#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDebug>
#include <QMainWindow>
#include <QTimer>

#include "statisticwindow.h"
#include "timecounter.h"



namespace Ui {
class MainWindow;
}

/**
 * @brief Главное окно. Отображается при открытии программы.
 */

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
    ///Если выбрано отображение счетчика отработанного времени, то должен
    /// выполняться каждую секунду. Перерисовывает счетчик отработанного
    /// времени.
    void on_timer();

    /**
     * @brief Устанавливает, нужно ли отображать счетчик отработанного времени.
     * @param arg1 == Qt::Checked - отображать, arg1 == Qt::Unchecked - нет.
     */
    void on_checkBox_stateChanged(int arg1);

    ///Позволяет вручную выставлять, требуется ли вычитать обед для счетчика
    /// отработанного времени. Если вычитать обед не получится (прошло времени
    /// меньше, чем длится обед), переключатель на форме не переключится.
    /// Влияет только на отображение.
    void on_checkBox_2_clicked();

    ///Выход. Закроет форму.
    void on_pushButton_clicked();

    ///Показать окно статистики.
    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
    StatisticWindow *statistic_window;

    TimeCounter *time_counter;
    QTimer *timer;
};

#endif // MAINWINDOW_H
