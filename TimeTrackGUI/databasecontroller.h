#ifndef DATABASECONTROLLER_H
#define DATABASECONTROLLER_H

#include <QDateTime>
#include <QDebug>
#include <QtSql>

#include "workingday.h"
#include "workingmonth.h"


/**
 * @brief Класс DataBaseController содержит в себе данные.
 *
 * @detailed Реализуется в виде Singleton. Для обращения к методам класса
 * сначала необходимо получить Instance: DataBaseController::Instance().
 *
 * База данных будет прочитана только при вызове readDataBase(). Этот метод
 * следует вызывать при каждом изменении данных.
 */

class DataBaseController : QObject
{
    Q_OBJECT

public:
    ///Используется Singleton. Для использования методов необходимо получить
    /// Instance: DataBaseController::Instance().
    static DataBaseController& Instance()
    {
      static DataBaseController singleton;
      return singleton;
    }

    /**
     * @brief Изменить день, дата которого совпадает с датой в started. День
     * будет изменен в соответствии с входными параметрами.
     * @param started - начало отработанного дня.
     * @param finished - конец отработанного дня.
     * @param need_to_work_msecs - сколько нужно отработать за день
     * (в миллисекундах). При отрицательных значениях берется значение по
     * умолчанию (Settings::Instance().get_need_to_work() = 8-ми часам).
     * @retval 0  - операция завершена успешно.
     * @retval -1 - ошибка: не удалось изменить базу данных.
     */
    int updateRecord(QDateTime started, QDateTime finished, int need_to_work_msecs = -1);

    ///Количество дней (записей).
    int getDaysCount()   {return working_days.count();  }

    ///Количество месяцев (за весь период).
    int getMonthsCount() {return working_months.count();}


    ///Дата i-го дня (элемента).
    QString getDateStr      (int i)  {return working_days[i].getDate().toString(Settings::Instance().getDateFormat());        }

    ///Время начала работы i-го дня (элемента).
    QString getStartedStr   (int i)  {return working_days[i].getStartedTime().toString(Settings::Instance().getTimeFormat()); }

    ///Время окончания работы i-го дня (элемента).
    QString getFinishedStr  (int i)  {return working_days[i].getFinishedTime().toString(Settings::Instance().getTimeFormat());}

    ///Отработанное время i-го дня (элемента).
    QString getWorkedStr    (int i)  {return working_days[i].getWorkedTime().toString(Settings::Instance().getTimeFormat());  }

    ///Время, которое нужно отработать (для i-го дня (элемента)).
    QString getNeedToWorkStr(int i)  {return QTime::fromMSecsSinceStartOfDay(working_days[i].getNeedToWork()).toString(Settings::Instance().getTimeFormat());}

    ///Переработка для i-го дня (элемента).
    QString getCurrentOver  (int i);

    ///Переработка для всех элементов за период (за месяц или за все время, в
    /// зависимости от _calculate_only_current_month).
    QString getTotalOver();

    ///Название месяца + год (для i-го месяца в списке).
    QString getMonthCaptionStr   (int i) {return working_months[i].getMonthCaption(); }

    ///Сколько отработано за месяц: подготовленная строка (часы, минуты, рабочие дни).
    QString getMonthWorkedTimeStr(int i) {return working_months[i].getWorkedTimeStr();}


    /**
     * @brief Переключить учет периода: обсчитывать только текущий месяц или
     * учитывать все отработанные дни (за все время). База перечитается
     * автоматически.
     * @param calculate_only_current_month = true - если требуется учитывать
     * только текущий месяц; calculate_only_current_month = false - для учета
     * всех отработанных дней (за все время).
     */
    void setOnlyCurrentMonth(bool calculate_only_current_month)
    {
        if (_calculate_only_current_month != calculate_only_current_month)
        {
            _calculate_only_current_month = calculate_only_current_month;
            readDataBase();
        }
    }

    ///Добавить в базу пустой день (для его последующего редактирования). Будет
    /// добавлен день с отработанным обедом (чтобы переработка была -8 часов).
    void addEmptyDay(const QDate &date);

    /**
     * @brief Удаляет день из базы данных по указанной дате.
     * @param date - дата того дня, который требуется удалить.
     * @retval 0  - операция завершена успешно.
     * @retval -1 - ошибка: не удалось найти день по указанной дате.
     * @retval -2 - ошибка: не удалось изменить базу данных.
     */
    int  deleteDay(const QDate &date);


private:
    ///Запрет вызова конструктора (т.к. реализуем Singleton).
    DataBaseController();

    ///Запрет вызова деструктора (т.к. реализуем Singleton).
    ~DataBaseController();

    ///Запрет вызова конструктора копирования.
    DataBaseController(const DataBaseController&);

    ///Запрет присваивания.
    DataBaseController& operator=(const DataBaseController&);


    ///Признак учета только текущего месяца (не влияет на статистику по месяцам).
    bool _calculate_only_current_month;

    ///Общая переработка (в миллисекундах).
    int  over_total_msecs;


    ///Список из всех отработанных дней за рассматриваемый период (за месяц или
    /// за все время, в зависимости от _calculate_only_current_month).
    QList<WorkingDay>   working_days;

    ///Список из отработанных месяцев.
    QList<WorkingMonth> working_months;


    ///Считывает базу данных (SQLite), переводит ее во внутреннее
    /// представл
    ///
    ///
    ///
    ///
    ///
    ///
    ///
    /// ение - список из WorkingDay - working_days. В зависимости от
    /// _calculate_only_current_month считает все данные или только текущий
    /// месяц.
    void readDataBase();

    ///Расчитвает переботку за весь период (за месяц или за все время:
    /// обсчитывается всё содержимое working_days).
    void calculate_total_over();

    ///Заполняет список working_months (из списка working_days).
    void fill_working_months();
};

#endif // DATABASECONTROLLER_H
