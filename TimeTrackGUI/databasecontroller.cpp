#include "databasecontroller.h"

DataBaseController::DataBaseController():
    _calculate_only_current_month(true),
    over_total_msecs(0)
{
    readDataBase();
}


DataBaseController::~DataBaseController()
{
    working_days.clear();
    working_months.clear();
}



void DataBaseController::readDataBase()
{
    working_days.clear();

    QSqlQuery a_query; //Для запросов SQL

    if (!a_query.exec("SELECT id, start, end, need_to_work FROM TimeTrackTable"))
    {
        qDebug() << "Can't " <<  a_query.lastQuery();
        qDebug() << "Probably, can't find right (non-zero size) file with database.";
        return;
    }

    //now we need to calculate begin and end of current month in MSecs
    //warning: dark magic here
    QDateTime month_start = QDateTime::currentDateTime().addDays(-1*(QDate::currentDate().day()-1));
    month_start.setTime(QTime(0,0,0));

    QDateTime month_end = month_start.addMonths(1);
    qint64 month_start_msecs = month_start.toMSecsSinceEpoch();
    qint64 month_end_msecs = month_end.toMSecsSinceEpoch();

    qint64 started, finished;
    int id;
    int need_to_work;
    while (a_query.next())
    {
        started = a_query.value(a_query.record().indexOf("start")).toLongLong();
        finished = a_query.value(a_query.record().indexOf("end")).toLongLong();

        if (_calculate_only_current_month)
        {
            //skipping previous months (and following too)
            if ((started < month_start_msecs) || (finished >= month_end_msecs)) continue;
        }

        id = a_query.value(a_query.record().indexOf("id")).toInt();

        if (a_query.value(a_query.record().indexOf("need_to_work")).isNull())
        {
            need_to_work = Settings::Instance().get_need_to_work_msecs();
        }
        else
        {
            need_to_work = a_query.value(a_query.record().indexOf("need_to_work")).toInt();
        }

        working_days.append(WorkingDay(id,
                                       QDateTime::fromMSecsSinceEpoch(started),
                                       QDateTime::fromMSecsSinceEpoch(finished),
                                       need_to_work
                                       )
                            );
    }
    qSort(working_days);
    calculate_total_over();
    fill_working_months();
}



void DataBaseController::calculate_total_over()
{
    over_total_msecs = 0;

    //skiping the last day
    for (int i=0; i<working_days.count()-1; i++)
    {
        over_total_msecs += working_days[i].getWorkedTime().msecsSinceStartOfDay() - working_days[i].getNeedToWork();
    }
}



void DataBaseController::fill_working_months()
{
    if (working_days.count() <= 0) return;

    working_months.clear();
    working_months.append(WorkingMonth(working_days[0]));

    for (int i=1; i<working_days.count(); i++)
    {
        if ((working_days[i].getDate().year() == working_months.last().getYear()) &&
                (working_days[i].getDate().month() == working_months.last().getMonth()))
            working_months.last().addDay(working_days[i]);
        else working_months.append(WorkingMonth(working_days[i]));
    }
}



int DataBaseController::updateRecord(QDateTime started, QDateTime finished, int need_to_work_msecs)
{
    if (need_to_work_msecs < 0) need_to_work_msecs = Settings::Instance().get_need_to_work_msecs();

    int id = -1;
    for (int i=0; i<working_days.count(); i++)
    {
        if (working_days[i].getDate() == started.date())
        {
            id = working_days[i].getID();

            //replace the record in memory
            working_days.removeAt(i);
            working_days.append(WorkingDay(id, started, finished, need_to_work_msecs));
            qSort(working_days);
            calculate_total_over();
            fill_working_months();

            break;
        }
    }

    QSqlQuery a_query; //Для запросов SQL
    //update the record in database
    a_query.prepare("UPDATE TimeTrackTable SET start = :start, end = :end, need_to_work = :need_to_work WHERE id = :id;");
    a_query.bindValue(":start",        started.toMSecsSinceEpoch());
    a_query.bindValue(":end",          finished.toMSecsSinceEpoch());
    a_query.bindValue(":need_to_work", need_to_work_msecs);
    a_query.bindValue(":id",           id);

    if (!a_query.exec())
    {
        qDebug() << "Can't update table.";
        return -1;
    }

    return 0;
}



QString DataBaseController::getCurrentOver(int i)
{
    int   over_msecs = working_days[i].getWorkedTime().msecsSinceStartOfDay() - working_days[i].getNeedToWork();
    QTime over_time = QTime::fromMSecsSinceStartOfDay( abs(over_msecs) );
    char  sign;
    if (over_msecs < 0) sign = '-'; else sign = '+';
    return (sign + over_time.toString(Settings::Instance().getTimeFormat()));
}



QString DataBaseController::getTotalOver()
{
    QTime over_total_time = QTime::fromMSecsSinceStartOfDay( abs(over_total_msecs) );
    char  sign;
    if (over_total_msecs < 0) sign = '-'; else sign = '+';
    return (sign + over_total_time.toString(Settings::Instance().getTimeFormat()));
}



void DataBaseController::addEmptyDay(const QDate &date)
{
    QDateTime started(date);
    started.setTime(QTime(9,30));

    QDateTime finished = started.addMSecs(Settings::Instance().get_dinner_duration().msecsSinceStartOfDay());

    //get new id
    QSqlQuery a_query; //Для запросов SQL
    a_query.exec("SELECT id FROM TimeTrackTable");
    a_query.last();
    int id = a_query.value(a_query.record().indexOf("id")).toInt();
    id++;

    working_days.append(WorkingDay(id, started, finished, Settings::Instance().get_need_to_work_msecs()));
    qSort(working_days);
    calculate_total_over();
    fill_working_months();

    //write new day in database
    a_query.prepare("INSERT INTO TimeTrackTable (id, start, end, need_to_work) "
                    "VALUES (:id, :start, :end, :need_to_work);");
    a_query.bindValue(":id",    id);
    a_query.bindValue(":start", started.toMSecsSinceEpoch());
    a_query.bindValue(":end",   finished.toMSecsSinceEpoch());
    a_query.bindValue(":need_to_work", Settings::Instance().get_need_to_work_msecs());

    if (!a_query.exec())
    {
        qDebug() << "Can't update table. SQL query = " + a_query.lastQuery();
    }
}



int DataBaseController::deleteDay(const QDate &date)
{
    //search id for deleting day
    int id = -1;
    for (int i=0; i<working_days.count(); i++)
    {
        if (working_days[i].getDate() == date)
        {
            id = working_days[i].getID();

            working_days.removeAt(i);
            calculate_total_over();
            fill_working_months();

            break;
        }
    }
    if (id == -1) return -1;  //can't find the day to delete

    QSqlQuery a_query; //Для запросов SQL
    //now delete the day by id
    a_query.prepare("DELETE FROM TimeTrackTable WHERE id= :id;");
    a_query.bindValue(":id", id);
    if (!a_query.exec())
    {
        qDebug() << "Can't update table. SQL query = " + a_query.lastQuery();
        return -2;
    }
    return 0;
}

