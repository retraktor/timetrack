#include "settings.h"

#include <QCoreApplication>


Settings::Settings() :
    qsettings      ("./.time_track_config", QSettings::IniFormat),
    database_path  ("./data.sqlite"),
    sdb            (QSqlDatabase::addDatabase("QSQLITE")),
    dinner_duration(0, 45),                                //обед 45 минут
    need_to_work   (QTime(8,0,0).msecsSinceStartOfDay()),  //рабочий день 8 часов (после вычитания обеда)
    time_format("hh:mm"),
    date_format("dd.MM.yyyy")
{
    QString database_path_from_qsettings = qsettings.value("database_path").toString();
    if (database_path_from_qsettings != "") database_path = database_path_from_qsettings;
    else
    {
        qsettings.setValue("database_path", database_path);
        qsettings.sync();
    }

    sdb.setDatabaseName(database_path);

    if (!sdb.open())
    {
        qDebug() << "Something wrong. ";
        qDebug() << sdb.lastError().text();
        return ;
    }
}

