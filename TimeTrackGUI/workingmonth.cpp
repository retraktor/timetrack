#include "workingmonth.h"

WorkingMonth::WorkingMonth(WorkingDay &day) :
    month(day.getDate().month()),
    year(day.getDate().year())
{
    working_days.append(day);
}


int WorkingMonth::addDay(WorkingDay &day)
{
    if (working_days.isEmpty())
    {
        month = day.getDate().month();
        year = day.getDate().year();
    }
    else if ((month != day.getDate().month()) || (year != day.getDate().year()))
    {
        return -1;
    }

    working_days.append(day);
    return 0;
}



qint64 WorkingMonth::getWorkedTime()
{
    qint64 worked_time(0);
    for (int i=0; i<working_days.count(); i++)
    {
        worked_time += working_days[i].getWorkedTime().second() +
                       working_days[i].getWorkedTime().minute() * 60 +
                       working_days[i].getWorkedTime().hour() * 60*60;
    }
    return worked_time;
}



QString WorkingMonth::getMonthCaption()
{
    QString str;
    switch (month)
    {
        case 1:  str = "Январь";   break;
        case 2:  str = "Февраль";  break;
        case 3:  str = "Март";     break;
        case 4:  str = "Апрель";   break;
        case 5:  str = "Май";      break;
        case 6:  str = "Июнь";     break;
        case 7:  str = "Июль";     break;
        case 8:  str = "Август";   break;
        case 9:  str = "Сентябрь"; break;
        case 10: str = "Октябрь";  break;
        case 11: str = "Ноябрь";   break;
        case 12: str = "Декабрь";  break;
        default: return "unknown"; break;
    }
    str += " " + QString::number(year);
    return str;
}



QString WorkingMonth::getWorkedTimeStr()
{
    qint64 worked_s(getWorkedTime());
    qint64 worked_s_2 = worked_s;

    int hours = worked_s / (60*60);
    worked_s -= hours*60*60;
    int minutes = worked_s / 60;

    int days = worked_s_2 / (8*60*60);
    worked_s_2 -= days*8*60*60;
    int hours_2 = worked_s_2 / (60*60);

    return QString::number(hours)   + " часов " +
           QString::number(minutes) + " минут " +
           "(" + QString::number(days) + " р.д. " +
           QString::number(hours_2)    + " часов)";
}
