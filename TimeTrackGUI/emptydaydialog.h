#ifndef EMPTYDAYDIALOG_H
#define EMPTYDAYDIALOG_H

#include <QDialog>


namespace Ui {
class EmptyDayDialog;
}


/**
 * @brief Диалоговое окно для ввода даты. Содержит кнопку и поле ввода даты.
 * Используется для добавления нового дня.
 */

class EmptyDayDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EmptyDayDialog(QWidget *parent = 0);
    ~EmptyDayDialog();

signals:
    void selected_date(const QDate&);

private slots:
    void on_buttonBox_accepted();

private:
    Ui::EmptyDayDialog *ui;
};

#endif // EMPTYDAYDIALOG_H
