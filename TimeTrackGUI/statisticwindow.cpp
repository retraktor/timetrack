#include "statisticwindow.h"
#include "ui_statisticwindow.h"


#include "settings.h"


StatisticWindow::StatisticWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StatisticWindow),
    date_str("Дата"),
    started_str("Начало"),
    finished_str("Конец"),
    need_to_work_str("Надо отработать"),
    table_prepared(false)
{
    ui->setupUi(this);
    ui->label->setVisible(false);
    ui->label->setStyleSheet("QLabel { background-color : green; color : white; }");
    connect(ui->cb_show_in_out,   SIGNAL(clicked()), this, SLOT(refillTable()));
    connect(ui->cb_current_month, SIGNAL(clicked()), this, SLOT(refillTable()));
    connect(ui->cb_show_overs,    SIGNAL(clicked()), this, SLOT(refillTable()));
}



StatisticWindow::~StatisticWindow()
{
    delete ui;
}



void StatisticWindow::fillTableWithDates()
{
    ui->tableWidget->setColumnCount(ui->tableWidget->columnCount() + 1);
    addTableItem(-1, ui->tableWidget->columnCount()-1, date_str, true, true);

    ui->tableWidget->setRowCount(DataBaseController::Instance().getDaysCount());

    for (int i=0; i<DataBaseController::Instance().getDaysCount(); i++)
    {
        addTableItem(i, ui->tableWidget->columnCount()-1, DataBaseController::Instance().getDateStr(i), true);
    }
}



void StatisticWindow::fillTableWithSessions()
{
    ui->tableWidget->setColumnCount(ui->tableWidget->columnCount() + 2);
    addTableItem(-1, ui->tableWidget->columnCount()-2, started_str, true, true);
    addTableItem(-1, ui->tableWidget->columnCount()-1, finished_str, true, true);

    ui->tableWidget->setRowCount(DataBaseController::Instance().getDaysCount());
    for (int i=0; i < DataBaseController::Instance().getDaysCount(); i++)
    {
        addTableItem(i, ui->tableWidget->columnCount()-2, DataBaseController::Instance().getStartedStr(i),  false);
        addTableItem(i, ui->tableWidget->columnCount()-1, DataBaseController::Instance().getFinishedStr(i), false);
    }
}



void StatisticWindow::fillTableWithDurations()
{
    ui->tableWidget->setColumnCount(ui->tableWidget->columnCount()+1);
    addTableItem(-1, ui->tableWidget->columnCount()-1, "Длительность", true, true);

    ui->tableWidget->setRowCount(DataBaseController::Instance().getDaysCount());
    for (int i=0; i<DataBaseController::Instance().getDaysCount(); i++)
    {
        addTableItem(i, ui->tableWidget->columnCount()-1, DataBaseController::Instance().getWorkedStr(i), true);
    }
}



void StatisticWindow::fillTableWithMonths()
{
    ui->tableWidget->setColumnCount(ui->tableWidget->columnCount()+2);
    ui->tableWidget->setColumnWidth(ui->tableWidget->columnCount()-1, 350);
    addTableItem(-1, ui->tableWidget->columnCount()-2, "Месяц", true, true);
    addTableItem(-1, ui->tableWidget->columnCount()-1, "Отработано за месяц (1 рабочий день = 8 часов)", true, true);

    ui->tableWidget->setRowCount(DataBaseController::Instance().getMonthsCount());
    for (int i=0; i<DataBaseController::Instance().getMonthsCount(); i++)
    {
        addTableItem(i, ui->tableWidget->columnCount()-2, DataBaseController::Instance().getMonthCaptionStr(i),   true);
        addTableItem(i, ui->tableWidget->columnCount()-1, DataBaseController::Instance().getMonthWorkedTimeStr(i), true);
    }
}



void StatisticWindow::fillTableWithOvers()
{
    ui->tableWidget->setColumnCount(ui->tableWidget->columnCount() + 2);
    ui->tableWidget->setColumnWidth(ui->tableWidget->columnCount()-2, 110);
    addTableItem(-1, ui->tableWidget->columnCount()-2, need_to_work_str, true, true);
    addTableItem(-1, ui->tableWidget->columnCount()-1, "Переработка", true, true);

    ui->tableWidget->setRowCount(DataBaseController::Instance().getDaysCount());

    for (int i=0; i < DataBaseController::Instance().getDaysCount(); i++)
    {
        addTableItem(i, ui->tableWidget->columnCount()-2, DataBaseController::Instance().getNeedToWorkStr(i), false);
        if (i == DataBaseController::Instance().getDaysCount()-1) break;  //don't consider current day

        addTableItem(i, ui->tableWidget->columnCount()-1, DataBaseController::Instance().getCurrentOver(i), true);
    }

    ui->tableWidget->setRowCount(ui->tableWidget->rowCount() + 1);
    addTableItem(ui->tableWidget->rowCount()-1, ui->tableWidget->columnCount()-1, DataBaseController::Instance().getTotalOver(), true);
}



void StatisticWindow::cleanTable()
{
    table_prepared = false;
    ui->label->setVisible(false);

    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(1);
    ui->tableWidget->setColumnCount(0);
}



void StatisticWindow::refillTable()
{
    cleanTable();

    if (ui->cb_month_statistic->checkState() == Qt::Unchecked)
    {
        fillTableWithDates();
        if (ui->cb_show_in_out->isChecked()) fillTableWithSessions();
        fillTableWithDurations();
        if (ui->cb_show_overs->isChecked()) fillTableWithOvers();
    }
    else
    {
        fillTableWithMonths();
    }
    table_prepared = true;
}



void StatisticWindow::addEmptyDay(const QDate &date)
{
    DataBaseController::Instance().addEmptyDay(date);
    refillTable();
}



void StatisticWindow::showEvent(QShowEvent *ev)
//выполнится только при показе этой формы. Возможно потому, что при показе первой (MainWindow) она еще не сконструерована
// (new StatisticWindow выполняется по кнопке)
{
    refillTable();
    ev->accept();
}



void StatisticWindow::closeEvent(QCloseEvent *ev)
//выполнится при закрытии любой из форм. Если закрывать одну за другой - выполнится дважды
{
    cleanTable();
    ev->accept();
}



void StatisticWindow::on_cb_month_statistic_stateChanged(int arg1)
{
    if (arg1 == Qt::Unchecked)
    {
        ui->cb_show_in_out->setVisible(true);
        ui->cb_current_month->setVisible(true);
        ui->pb_write_to_database->setVisible(true);
        ui->cb_show_overs->setVisible(true);
        ui->pb_add_empty_day->setVisible(true);
        ui->pb_delete_day->setVisible(true);

        bool only_current_month;
        if (ui->cb_current_month->checkState() == Qt::Checked) only_current_month = true;
        else only_current_month = false;
        DataBaseController::Instance().setOnlyCurrentMonth(only_current_month);
    }
    else
    {
        ui->cb_show_in_out->setVisible(false);
        ui->cb_current_month->setVisible(false);
        ui->pb_write_to_database->setVisible(false);
        ui->cb_show_overs->setVisible(false);
        ui->pb_add_empty_day->setVisible(false);
        ui->pb_delete_day->setVisible(false);
        DataBaseController::Instance().setOnlyCurrentMonth(false);
    }
    refillTable();
}



void StatisticWindow::addTableItem(int row, int col, QString str, bool read_only, bool highlighting)
{
    QTableWidgetItem *item(new QTableWidgetItem(str));
    if (read_only) item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled); //set it "read only"
    if (highlighting)
    {
        QFont font;
        font.setBold(true);
        item->setFont(font);
    }
    if (row < 0) ui->tableWidget->setHorizontalHeaderItem(col, item);
    else ui->tableWidget->setItem(row, col, item);
    //here is no memory leaks (see http://stackoverflow.com/questions/21997025/qtablewidget-memory-leak-or-not )
}




void StatisticWindow::on_pb_write_to_database_clicked()
{
    if (!(ui->cb_show_in_out->isChecked())) return;
    if (!((ui->tableWidget->horizontalHeaderItem(1)->text()==started_str) &&
          (ui->tableWidget->horizontalHeaderItem(2)->text()==finished_str))) return;


    int need_to_work_column = -1; //number of column with "need to work"
    if (ui->cb_show_overs->isChecked())
    {
        for (int i=0; i < ui->tableWidget->columnCount(); i++)
        {
            if (ui->tableWidget->horizontalHeaderItem(i)->text() == need_to_work_str)
            {
                need_to_work_column = i;
                break;
            }
        }
    }

    for (int current_row=0; current_row < ui->tableWidget->rowCount(); current_row++)
    {
        if ((!ui->tableWidget->item(current_row,0)) || (!ui->tableWidget->item(current_row,1)) || (!ui->tableWidget->item(current_row,2)))
        {
            break;
        }

        QDateTime started(QDate::fromString(ui->tableWidget->item(current_row,0)->text(), Settings::Instance().getDateFormat()),
                          QTime::fromString(ui->tableWidget->item(current_row,1)->text(), Settings::Instance().getTimeFormat()));

        QDateTime finished(QDate::fromString(ui->tableWidget->item(current_row,0)->text(), Settings::Instance().getDateFormat()),
                           QTime::fromString(ui->tableWidget->item(current_row,2)->text(), Settings::Instance().getTimeFormat()));

        if (need_to_work_column > 0)
        {
            if (!ui->tableWidget->item(current_row, need_to_work_column)) break;
            int need_to_work_msecs = QTime::fromString(ui->tableWidget->item(current_row, need_to_work_column)->text(),
                                                       Settings::Instance().getTimeFormat()
                                                       ).msecsSinceStartOfDay();
            if (DataBaseController::Instance().updateRecord(started, finished, need_to_work_msecs)) break;
        }
        else
        {
            if (DataBaseController::Instance().updateRecord(started, finished)) break;
        }
    }
    refillTable();
}



void StatisticWindow::on_tableWidget_itemChanged(QTableWidgetItem *item)
{
    item->column();  //to cancel warning
    if (table_prepared) ui->label->setVisible(true);
}



void StatisticWindow::on_pb_add_empty_day_clicked()
{
    EmptyDayDialog *edd = new EmptyDayDialog(this); //new window will be in parent window
    connect(edd, SIGNAL(selected_date(const QDate&)), this, SLOT(addEmptyDay(const QDate&)));
    edd->setAttribute(Qt::WA_DeleteOnClose);        //will free memory on window closing
    edd->show();
}



void StatisticWindow::on_pb_delete_day_clicked()
{
    //if no cell selected, exiting
    if (ui->tableWidget->selectedItems().empty()) return;

    //show modal dialog window (you can choose other window, but can't do anything)
    QString msg;
    if (ui->tableWidget->selectedItems().count() > 1) msg = "Удалить выбранные дни из базы данных?";
    else msg = "Удалить выбранный день из базы данных?";
    int res = QMessageBox::warning(0, " ", msg, QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

    if (res == QMessageBox::Yes)
    {
        //collect rows to delete
        QList<int> rows_to_delete;
        foreach (QTableWidgetItem* twi, ui->tableWidget->selectedItems())
        {
            if (rows_to_delete.indexOf(twi->row()) < 0) rows_to_delete.append(twi->row()); //skip dublicates
        }

        //search column with date
        int column_with_date;
        for (int i=0; i<ui->tableWidget->columnCount(); i++)
        {
            if (ui->tableWidget->horizontalHeaderItem(i)->text() == date_str)
            {
                column_with_date = i;
                break;
            }
        }

        foreach (int row, rows_to_delete)
        {
            DataBaseController::Instance().deleteDay(
                        QDate::fromString( ui->tableWidget->item(row, column_with_date)->text(),
                                           Settings::Instance().getDateFormat()
                                           )
                        );
        }
        refillTable();
    }
}


void StatisticWindow::on_cb_current_month_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked) DataBaseController::Instance().setOnlyCurrentMonth(true);
    else                     DataBaseController::Instance().setOnlyCurrentMonth(false);
}
