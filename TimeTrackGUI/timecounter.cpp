#include "timecounter.h"

TimeCounter::TimeCounter(QObject *parent) :
    QObject(parent),
    started_time(QDateTime::currentDateTime()),
    need_to_subtract_dinner(false)
{
    Settings::Instance();//инициализируем Settings, чтобы получить доступ к базе данных

    QSqlQuery a_query;
    if (!a_query.exec("SELECT start FROM TimeTrackTable ORDER BY start"))
    {
        qDebug() << "Can't SELECT start, end FROM TimeTrackTable. ";
        qDebug() << "Probably, can't find right (non-zero size) file with database. ";
        qDebug() << "So, started time = current time. ";
        return;
    }
    a_query.last();
    started_time = QDateTime::fromMSecsSinceEpoch(a_query.value(a_query.record().indexOf("start")).toLongLong());
}



bool TimeCounter::set_NeedToSubtractDinner(bool value)
{
    if (!((workedTime() < Settings::Instance().get_dinner_duration()) && (value == true)))
    {
        need_to_subtract_dinner = value;
    }
    else
    {
        qDebug() << "Прошло слишком мало времени, чтобы еще и обед вычитать";
    }
    return need_to_subtract_dinner;
}



QTime TimeCounter::timeToGoAway()
{
    //some magic here :)
    return (QTime::fromMSecsSinceStartOfDay(
                     ((started_time.time().msecsSinceStartOfDay() +
                       Settings::Instance().get_need_to_work_msecs() +
                       Settings::Instance().get_dinner_duration_msecs())) % QTime(23,59).msecsSinceStartOfDay())
                 );
    //берем остаток от деления, чтобы организовать переход через 00:00:00
}



QString TimeCounter::workedToday()
{
    QTime time;
    if (need_to_subtract_dinner)
    {
        time = QTime::fromMSecsSinceStartOfDay(workedTime().msecsSinceStartOfDay() -
                                               Settings::Instance().get_dinner_duration_msecs()
                                               );
    }
    else
    {
        time = workedTime();
    }
    return time.toString("hh:mm:ss");
}



bool TimeCounter::analyzeIsItNeededToSubtractDinner()
{
    QDateTime current_time(QDateTime::currentDateTime());

    //рекомендация по вычету обеда вносится после 13:00 и только если отработано более часа
    if ((workedTime() < QTime(1,0)) || (current_time.time() < QTime(13,0)))
    {
        need_to_subtract_dinner = false;
    }
    else
    {
        need_to_subtract_dinner = true;
    }
    return need_to_subtract_dinner;
}
