#ifndef TIMECOUNTER_H
#define TIMECOUNTER_H

#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QObject>
#include <QtSql>

#include "settings.h"


/**
 * @brief Считчик отработанного времени.
 * @detailed Нужен только для отображения рекомендуемого времени ухода и
 * отработанного времени. Не влияет на базу данных и на статистику.
 */

class TimeCounter : public QObject
{
    Q_OBJECT
public:
    ///В конструкторе возьмем из базы данных начало текущего рабочего дня.
    explicit TimeCounter(QObject *parent = 0);


    /**
     * @brief Установить, нужно ли вычитать обед (только для счетчика
     * отработанного времени). Влияет только на отображение.
     * @param value = true - вычитать; value = false - не вычитать.
     * @return Вернет, будет ли вычитаться обед (т.к. если вычитать не получится,
     * установка вычета обеда не сработает).

     */
    bool set_NeedToSubtractDinner(bool value); //вернет значение, в которое был установлено need_to_subtract_dinner

    ///Вернет рекомендуемое время окончания работы (с расчетом, чтобы отработать
    /// 8 часов).
    QTime timeToGoAway();



public slots:
    ///Вернет, сколько отработали за сегодня.
    QString workedToday();

    /**
     * @brief Произвести анализ, не пора ли вычитать обед (только для счетчика
     * отработанного времени). По результатам проверки выставит соответствующую
     * внутреннюю переменную.
     * @retval true - обед будет вычитаться.
     * @retval false - обед не будет вычитаться.
     */
    bool analyzeIsItNeededToSubtractDinner();  //вернет значение, в которое был установлено need_to_subtract_dinner


private:
    ///Начало текущего рабочего дня.
    QDateTime started_time;

    ///Нужно ли вычитать обед. Влияет только на отображение.
    bool need_to_subtract_dinner;


    ///Отработанно за сегодня.
    QTime workedTime()
    {
        return QTime::fromMSecsSinceStartOfDay(
                    QDateTime::currentDateTime().toMSecsSinceEpoch() -
                    started_time.toMSecsSinceEpoch()
                    );
    }
};

#endif // TIMECOUNTER_H
