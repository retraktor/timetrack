#include <QCoreApplication>
#include <QDateTime>
#include <QSettings>
#include <QSystemSemaphore>
#include <QtSql>



int main(int argc, char *argv[])
{
    //to protect from two and more parallel daemons running
    QSystemSemaphore semaphore("TimeTrackDaemon", 1, QSystemSemaphore::Open);
    semaphore.acquire();

    QCoreApplication a(argc, argv);
    QString database_path("./data.sqlite");
    int last_id = 0;
    qint64 started_time;    
    int need_to_work = QTime(8,0,0).msecsSinceStartOfDay();

    QSettings qsettings("./.time_track_config", QSettings::IniFormat);
    QString database_path_from_qsettings = qsettings.value("database_path").toString();
    if (database_path_from_qsettings != "") database_path = database_path_from_qsettings;
    else
    {
        qsettings.setValue("database_path", database_path);
        qsettings.sync();
    }


    QSqlDatabase sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName(database_path);

    if (!sdb.open())
    {
        qDebug() << "Something wrong. ";
        qDebug() << sdb.lastError().text();
        return -1;
    }

    QSqlQuery a_query;

    if (!a_query.exec("SELECT * FROM TimeTrackTable"))
    {
        qDebug() << "No data found. Creating new table. ";
        QString str = "CREATE TABLE TimeTrackTable ("
                                             "id integer PRIMARY KEY NOT NULL, "
                                             "start integer, "
                                             "end integer, "
                                             "need_to_work"
                                             ");";
        if (a_query.exec(str))
        {
            last_id = 1;
            started_time = QDateTime::currentDateTime().toMSecsSinceEpoch();
            a_query.prepare("INSERT INTO TimeTrackTable (id, start, end, need_to_work)"
                                          "VALUES (:id, :start, :end, :need_to_work);");
            a_query.bindValue(":id", last_id);
            a_query.bindValue(":start", started_time);
            a_query.bindValue(":end", started_time);
            a_query.bindValue(":need_to_work", need_to_work);
            if (a_query.exec())
            {
                qDebug() << "Creating new table ok.";
            }
            else
            {
                qDebug() << "Fatal error. Can't INSERT first record into new table.";
                return -1;
            }
        }
        else
        {
            qDebug() << "Fatal error. Can't create new table.";
            return -1;
        }
    }

    if (!a_query.exec("SELECT id, start FROM TimeTrackTable"))
    {
        qDebug() << "Can't " << a_query.lastQuery() << ". Can't continue, exiting.";
        return -1;
    }
    else
    {
        a_query.last();
        last_id = a_query.value(a_query.record().indexOf("id")).toInt();
        started_time = a_query.value(a_query.record().indexOf("start")).toLongLong();

        QDate start_date = QDateTime::fromMSecsSinceEpoch(started_time).date();
        QDate current_date = QDateTime::currentDateTime().date();

        if (start_date < current_date) //then start date is earlier than current date
        {
            //pasting new record in database
            ++last_id;
            started_time = QDateTime::currentDateTime().toMSecsSinceEpoch();
            a_query.prepare("INSERT INTO TimeTrackTable (id, start, end, need_to_work)"
                                          "VALUES (:id, :start, :end, :need_to_work);");
            a_query.bindValue(":id", last_id);
            a_query.bindValue(":start", started_time);
            a_query.bindValue(":end", started_time);
            a_query.bindValue(":need_to_work", need_to_work);
            a_query.exec();
        }
    }


    while(1)
    {
        QThread::sleep(5*60);  //pushin record every 5 min

        qint64 end_time = QDateTime::currentDateTime().toMSecsSinceEpoch();        

        a_query.prepare("UPDATE TimeTrackTable SET end = :end WHERE id = :id;");
        a_query.bindValue(":end", end_time);
        a_query.bindValue(":id", last_id);        
        if (!a_query.exec())
        {
            qDebug() << "Fatal error in while pushing. Exiting";
            break;
        }
    }

    semaphore.release();
    return -1;
}


