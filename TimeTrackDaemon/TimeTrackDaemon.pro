#-------------------------------------------------
#
# Project created by QtCreator 2015-03-19T11:00:37
#
#-------------------------------------------------

QT       += core sql

QT       -= gui

TARGET = TimeTrackDaemon
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
